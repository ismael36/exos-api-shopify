// src/index.ts
import express from 'express';
import Shopify, { ApiVersion, AuthQuery, DataType } from '@shopify/shopify-api';

const app = express();

const { API_KEY, API_SECRET_KEY, SCOPES, SHOP, HOST, PORT } = process.env;

Shopify.Context.initialize({
  API_KEY,
  API_SECRET_KEY,
  SCOPES: [SCOPES],
  HOST_NAME: HOST,
  IS_EMBEDDED_APP: false,
  API_VERSION: ApiVersion.October21
});

const client = new Shopify.Clients.Rest(SHOP, API_SECRET_KEY);

app.post('/:object_type/:object_id/metafields', async (req, res) => {
  try {
    await client.post({
      path: `${req.params.object_type}/${req.params.object_id}/metafields`,
      data: {"metafield": req.query},
      type: DataType.JSON,
    });
    res.sendStatus(200);
  } catch (error) {
    console.log(error);
    res.sendStatus(400);

  }
});

app.listen(PORT, () => {
  console.log('app listening on port ' + PORT);
});
